package com.example.list_message

class SampleData {
    companion object {
        val conversationSample = arrayListOf(Message("Android", "text1\nText2\nText3\nText3"),
            Message("Android", "text1\nText2\nText3\nText3"),
            Message("Android", "text1\nText2\nText3\nText3"),
            Message("Android", "text1\nText2\nText3\nText3"),
            Message("Android", "text1\nText2\nText3\nText3"))
    }
}